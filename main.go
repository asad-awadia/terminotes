package main

import (
	"log"
	"net/http"

	"github.com/blevesearch/bleve"
	"github.com/dgraph-io/badger/v2"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	uuid "github.com/satori/go.uuid"
)

func main() {
	index, err := getBleveIndex("/tmp/index")

	db, err := badger.Open(badger.DefaultOptions("/tmp/badger"))
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	terminote := terminote{db: db, index: index}

	e := echo.New()
	e.Use(middleware.Logger())

	e.GET("/", handleRoot)
	e.GET("/api/v1/keys", terminote.genKey)

	notesRoutes := e.Group("/api/v1/notes")
	notesRoutes.Use(terminote.validateKey)

	notesRoutes.GET("/", terminote.handleGetAllNotes)
	notesRoutes.GET("/:id", terminote.handleGetNoteByID)
	notesRoutes.GET("/search", terminote.handleSearchNote)

	notesRoutes.POST("/", terminote.handleAddNote)
	notesRoutes.PUT("/:id", terminote.handlePutNoteByID)

	notesRoutes.DELETE("/:id", terminote.handleDeleteNoteByID)

	e.Logger.Fatal(e.Start(":1323"))
}

func handleRoot(c echo.Context) error {
	return c.String(http.StatusOK, "welcome to terminotes")
}

func getUUID() uuid.UUID {
	return uuid.Must(uuid.NewV4(), nil)
}

func getBleveIndex(indexPath string) (bleve.Index, error) {
	bleveIdx, err := bleve.Open(indexPath)
	if err != nil {
		mapping := bleve.NewIndexMapping()
		bleveIdx, err = bleve.New(indexPath, mapping)
		if err != nil {
			return nil, err
		}
	}

	return bleveIdx, nil
}
