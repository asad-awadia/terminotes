package main

import (
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/blevesearch/bleve"
	"github.com/dgraph-io/badger/v2"
	"github.com/labstack/echo/v4"
)

func TestTerminoteAdd(t *testing.T) {
	terminote := setupTerminote()
	defer terminote.db.Close()
	n := &note{ID: getUUID(), Text: "test note", CreateTime: time.Now()}
	_, err := terminote.addNote(getEchoContext(), n)
	if err != nil {
		t.Fail()
	}
}

func TestTerminoteGet(t *testing.T) {
	terminote := setupTerminote()
	defer terminote.db.Close()
	n := &note{ID: getUUID(), Text: "test note", CreateTime: time.Now()}
	_, err := terminote.addNote(getEchoContext(), n)
	if err != nil {
		t.Fail()
	}
	if terminote.getNoteByID(getEchoContext(), n.ID.String()).ID != n.ID {
		t.Fail()

	}
}

func TestTerminoteDelete(t *testing.T) {
	terminote := setupTerminote()
	defer terminote.db.Close()
	n := &note{ID: getUUID(), Text: "test note", CreateTime: time.Now()}
	_, err := terminote.addNote(getEchoContext(), n)
	if err != nil {
		t.Fail()
	}
	terminote.deleteNoteByID(getEchoContext(), n.ID.String())
	if terminote.getNoteByID(getEchoContext(), n.ID.String()) != nil {
		t.Fail()
	}
}

func setupTerminote() terminote {
	index, _ := bleve.NewMemOnly(bleve.NewIndexMapping())
	opt := badger.DefaultOptions("").WithInMemory(true)
	db, _ := badger.Open(opt)
	return terminote{db: db, index: index}
}

func getEchoContext() echo.Context {
	e := echo.New()
	req := httptest.NewRequest(http.MethodPost, "/", nil)
	rec := httptest.NewRecorder()
	context := e.NewContext(req, rec)
	context.Set("auth-key", "foo")
	return context
}
