package main

import (
	"crypto/sha256"
	"encoding/binary"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"github.com/blevesearch/bleve"
	"github.com/dgraph-io/badger/v2"
	"github.com/labstack/echo/v4"
	uuid "github.com/satori/go.uuid"
)

//----------------------------------------------------------------------------------
// Service Struct definitions
//----------------------------------------------------------------------------------

type terminoteResponse struct {
	ResultCode int         `json:"result_code"`
	Data       interface{} `json:"data"`
	Error      string      `json:"error_message"`
}

type note struct {
	ID         uuid.UUID `json:"id"`
	Text       string    `json:"text"`
	CreateTime time.Time `json:"create_time"`
}

type terminote struct {
	db    *badger.DB
	index bleve.Index
}

var authKeyPrefix = []byte("auth:")
var notePrefix = []byte("note:")

//----------------------------------------------------------------------------------
// Auth Middleware
//----------------------------------------------------------------------------------

func (t terminote) genKey(c echo.Context) error {
	authKey := getUUID()
	k := append(authKeyPrefix, getHashedKey(authKey)...)
	err := t.db.Update(func(txn *badger.Txn) error {
		txn.Set(k, encodeTime(time.Now()))
		return nil
	})
	if err != nil {
		return c.JSON(http.StatusOK, terminoteResponse{ResultCode: http.StatusInternalServerError, Error: "could not generate auth key"})
	}
	return c.JSON(http.StatusOK, terminoteResponse{ResultCode: http.StatusOK, Data: authKey.String()})
}

func (t terminote) validateKey(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		authKey := c.Request().Header.Get("X-Terminote-Auth")
		authUID, _ := uuid.FromString(authKey)
		hashedAuthUID := getHashedKey(authUID)
		k := append(authKeyPrefix, hashedAuthUID...)
		err := t.db.View(func(txn *badger.Txn) error {
			_, err := txn.Get(k)
			return err
		})
		if err != nil {
			return c.JSON(http.StatusOK, terminoteResponse{ResultCode: http.StatusInternalServerError, Error: "could not validate auth key"})
		}
		c.Set("auth-key", hashedAuthUID)
		return next(c)
	}

}

//----------------------------------------------------------------------------------
// Router Handlers
//----------------------------------------------------------------------------------

func (t terminote) handleGetAllNotes(c echo.Context) error {
	listOfNotes := t.getAllNotes(c)
	if listOfNotes == nil {
		return c.JSON(http.StatusOK, terminoteResponse{ResultCode: http.StatusNotFound, Error: "no notes found"})
	}
	return c.JSON(http.StatusOK, terminoteResponse{ResultCode: http.StatusOK, Data: listOfNotes})
}

func (t terminote) handleGetNoteByID(c echo.Context) error {
	id := c.Param("id")
	note := t.getNoteByID(c, id)
	if note == nil {
		return c.JSON(http.StatusOK, terminoteResponse{ResultCode: http.StatusNotFound, Error: "no note found"})
	}
	return c.JSON(http.StatusOK, terminoteResponse{ResultCode: http.StatusOK, Data: note})
}

func (t terminote) handlePutNoteByID(c echo.Context) error {
	id := c.Param("id")
	note := &note{}
	c.Bind(note)
	err := t.updateNoteByID(c, id, note)
	if err != nil {
		return c.JSON(http.StatusOK, terminoteResponse{ResultCode: http.StatusInternalServerError, Error: err.Error()})
	}
	return c.JSON(http.StatusOK, terminoteResponse{ResultCode: http.StatusOK, Data: note})
}

func (t terminote) handleAddNote(c echo.Context) error {
	note := &note{}
	c.Bind(note)
	noteAdded, err := t.addNote(c, note)
	if err != nil {
		return c.JSON(http.StatusOK, terminoteResponse{ResultCode: http.StatusInternalServerError, Error: err.Error()})
	}
	err = t.index.Index(note.ID.String(), noteAdded)
	if err != nil {
		t.deleteNoteByID(c, noteAdded.ID.String())
		return c.JSON(http.StatusOK, terminoteResponse{ResultCode: http.StatusInternalServerError, Error: err.Error()})
	}
	return c.JSON(http.StatusOK, terminoteResponse{ResultCode: http.StatusOK, Data: noteAdded})
}

func (t terminote) handleDeleteNoteByID(c echo.Context) error {
	id := c.Param("id")
	t.deleteNoteByID(c, id)
	return c.JSON(http.StatusOK, terminoteResponse{ResultCode: http.StatusOK, Data: "note deleted"})
}

func (t terminote) handleSearchNote(c echo.Context) error {
	text := c.QueryParam("text")

	query := bleve.NewQueryStringQuery(text)
	search := bleve.NewSearchRequest(query)
	searchResults, err := t.index.Search(search)
	if err != nil {
		return c.JSON(http.StatusOK, terminoteResponse{ResultCode: http.StatusInternalServerError, Error: err.Error()})
	}
	count := searchResults.Hits.Len()
	if count == 0 {
		return c.JSON(http.StatusOK, terminoteResponse{ResultCode: http.StatusNotFound, Error: "no note found"})
	}
	var listOfNotes []*note
	for i := 0; i < count; i++ {
		listOfNotes = append(listOfNotes, t.getNoteByID(c, searchResults.Hits[i].ID))
	}
	return c.JSON(http.StatusOK, terminoteResponse{ResultCode: http.StatusOK, Data: listOfNotes})
}

//----------------------------------------------------------------------------------
// Service API methods
//----------------------------------------------------------------------------------

func (t terminote) getAllNotes(c echo.Context) []*note {
	var listOfNotes []*note
	t.db.View(func(txn *badger.Txn) error {
		opts := badger.DefaultIteratorOptions
		opts.PrefetchSize = 8
		it := txn.NewIterator(opts)
		defer it.Close()
		prefixForAuthKey := getNotePrefixForAuthKey(getAuthKeyFromContext(c))
		for it.Seek(prefixForAuthKey); it.ValidForPrefix(prefixForAuthKey); it.Next() {
			item := it.Item()
			err := item.Value(func(v []byte) error {
				noteData := note{}
				json.Unmarshal(v, &noteData)
				listOfNotes = append(listOfNotes, &noteData)
				return nil
			})
			if err != nil {
				return err
			}
		}
		return nil
	})
	return listOfNotes
}

func (t terminote) getNoteByID(c echo.Context, id string) *note {
	authKey := getAuthKeyFromContext(c)
	noteID, _ := uuid.FromString(id)
	var responseNote *note
	err := t.db.View(func(txn *badger.Txn) error {
		noteBytes, err := txn.Get(getNoteIDKey(authKey, noteID.Bytes()))
		if err != nil {
			return err
		}
		noteBytes.Value(func(val []byte) error {
			json.Unmarshal(val, &responseNote)
			return nil
		})
		return nil
	})
	if err != nil {
		return nil
	}
	return responseNote
}

func (t terminote) addNote(c echo.Context, note *note) (*note, error) {
	authKey := getAuthKeyFromContext(c)
	note.ID = getUUID()
	note.CreateTime = time.Now()
	noteBytes, _ := json.Marshal(note)
	err := t.db.Update(func(txn *badger.Txn) error {
		txn.Set(getNoteIDKey(authKey, note.ID.Bytes()), noteBytes)
		return nil
	})
	fmt.Println(err)
	return note, err
}

func (t terminote) updateNoteByID(c echo.Context, id string, note *note) error {
	authKey := getAuthKeyFromContext(c)
	noteBytes, _ := json.Marshal(note)
	return t.db.Update(func(txn *badger.Txn) error {
		txn.Set(getNoteIDKey(authKey, note.ID.Bytes()), noteBytes)
		return nil
	})
}

func (t terminote) deleteNoteByID(c echo.Context, id string) error {
	authKey := getAuthKeyFromContext(c)
	noteID, _ := uuid.FromString(id)
	return t.db.Update(func(txn *badger.Txn) error {
		txn.Delete(getNoteIDKey(authKey, noteID.Bytes()))
		return nil
	})
}

//----------------------------------------------------------------------------------
// Package helper methods
//----------------------------------------------------------------------------------

func encodeTime(t time.Time) []byte {
	buf := make([]byte, 8)
	u := uint64(t.Unix())
	binary.BigEndian.PutUint64(buf, u)
	return buf
}

func getAuthKeyFromContext(c echo.Context) []byte {
	authKey := c.Get("auth-key")
	hashedAuthKey, _ := authKey.([]byte)
	return hashedAuthKey
}

func getNoteIDKey(authKey []byte, noteID []byte) []byte {
	k := getNotePrefixForAuthKey(authKey)
	k = append(k, noteID...)
	return k
}

func getNotePrefixForAuthKey(authKey []byte) []byte {
	return append(notePrefix, authKey...)
}

func getHashedKey(key uuid.UUID) []byte {
	hasher := sha256.New()
	hasher.Write(key.Bytes())
	return hasher.Sum(nil)
}
