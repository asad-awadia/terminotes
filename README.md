# terminotes ✍️

Take notes directly from without ever having to leave your terminal

## What is terminotes?

Terminotes is an HTTP server that can be deployed to allow you to store and search notes in a centralised [accessible from home or the office] location directly from your terminal. It is similar to eureka [the rust notes project] but that saves it locally on the machine - this is saved on the server it is deployed to.

It has a backend server that can be deployed as a single go binary and invoked over HTTP

A command line tool is WIP

## What does terminotes use internally?

The HTTP server is built using [Echo](https://echo.labstack.com/), while the storage is done via [BadgerDB](https://dgraph.io/docs/badger) [embedded key value store by Dgraph]. The searching functionality is provided by [Bleve](https://blevesearch.com/) [search library used in couchbase]

## Who should use terminotes?

- You want to take notes directly from your terminal and have them be accessible from anywhere

- You want your notes/data to be stored on your own server

## API

### Unauthenticated routes

`GET /api/v1/keys` => Generate an API key - notes stored for this key will only be accessible by this key

### Authenticated routes

For the following routes a header with the API key needs to be present under `X-Terminote-Auth` key

API base prefix is `/api/v1/notes`

`GET /` => Get all notes

`POST /` => Create new note by ID

`GET /:id` => Get note by ID

`PUT /:id` => Put note by ID

`DELETE /:id` => Delete note by ID

`GET /search` => Search notes by query param `text` eg `/api/v1/notes/search?text=sushi%20party`

## API Model

```
Note:

{
    "id": "f460637a-d2e4-4124-930d-97b657410a67",
    "text": "go get groceries",
    "create_time": "2020-09-20T16:27:48.173581-04:00"
}

Response:

{
  "result_code": 200,
  "data": {
    "id": "f460637a-d2e4-4124-930d-97b657410a67",
    "text": "go get groceries",
    "create_time": "2020-09-20T16:27:48.173581-04:00"
  },
  "error_message": ""
}

{
  "result_code": 404,
  "data": null,
  "error_message": "no note found"
}
```

## Build

`go build main.go terminote.go`

## Run

`go run main.go terminote.go`

or

```
go build main.go terminote.go
./main
```
